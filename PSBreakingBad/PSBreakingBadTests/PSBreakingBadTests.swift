//
//  PSBreakingBadTests.swift
//  PSBreakingBadTests
//
//  Created by Shehab Saqib on 20/06/2021.
//

import XCTest
import Combine
@testable import PSBreakingBad

class TestModelData: XCTestCase {

    var cancellables = Set<AnyCancellable>()
    
    override func setUp() {
        cancellables = Set<AnyCancellable>()
    }
    
    func testEpisodes() {
        // Todo: Improve episodes view model as I wasn't able to test it directly.
        // Due to the injection of which series, find a different way around it.
        let useCase = EpisodesListUseCase(repo: MockEpisodesListRepo())
        
        var episodes: [Episodes] = []
        let episodesLoaded = expectation(description: "Received an array of episodes")
        
        useCase.getEpisodesListPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    XCTFail("Expected to decode correctly \(error)")
                default:
                    XCTAssertNotNil(episodes)
                }
            }, receiveValue: { allEpisodes in
                episodes = allEpisodes
                episodesLoaded.fulfill()
            })
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 2.0, handler: nil)
    }
    
    func testAllCharacters() {
        // Todo: Improve episodes view model as I wasn't able to test it directly.
        // Due to the injection of which series, find a different way around it.
        
        let useCase = CharacterListUseCase(repo: MockCharactersListRepo())
        
        var characters: [Character] = []
        let charactersLoaded = expectation(description: "Received an array of episodes")
        
        useCase.getCharacterListPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    XCTFail("Expected to decode correctly \(error)")
                default:
                    XCTAssertNotNil(characters)
                }
            }, receiveValue: { allCharacters in
                characters = allCharacters
                charactersLoaded.fulfill()
            })
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 2.0, handler: nil)
    }
    
    func testAllQuotes() {
        // Todo: Improve episodes view model as I wasn't able to test it directly.
        // Due to the injection of which series, find a different way around it.
        let useCase = QuoteListUseCase(repo: MockQuotesListRepo())
        
        var quotes: [Quote] = []
        let quotesLoaded = expectation(description: "Received an array of quotes")
        
        useCase.getQuoteListPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    XCTFail("Expected to decode correctly \(error)")
                default:
                    XCTAssertNotNil(quotes)
                }
            }, receiveValue: { allQuotes in
                quotes = allQuotes
                quotesLoaded.fulfill()
            })
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 2.0, handler: nil)
    }

}

struct MockEpisodesListRepo: EpisodesListRepository {
    func episodesListPublisher() -> AnyPublisher<[Episodes], HTTPError> {
        let url = fileURL(file: "BetterCallSaulEpisodes")
        let episodes = decode(
            [Episodes].self,
            from: url,
            keyDecodingStrategy: .convertFromSnakeCase
        )
        
        return Just(episodes)
            .setFailureType(to: HTTPError.self)
            .eraseToAnyPublisher()
    }
}

struct MockCharactersListRepo: CharacterListRepository {
    func characterListPublisher() -> AnyPublisher<[Character], HTTPError> {
        let url = fileURL(file: "AllCharacters")
        let characters = decode(
            [Character].self,
            from: url,
            keyDecodingStrategy: .convertFromSnakeCase
        )
        
        return Just(characters)
            .setFailureType(to: HTTPError.self)
            .eraseToAnyPublisher()
    }
}

struct MockQuotesListRepo: QuoteListRepository {
    func quoteListPublisher() -> AnyPublisher<[Quote], HTTPError> {
        let url = fileURL(file: "AllQuotes")
        let quotes = decode(
            [Quote].self,
            from: url,
            keyDecodingStrategy: .convertFromSnakeCase
        )
        
        return Just(quotes)
            .setFailureType(to: HTTPError.self)
            .eraseToAnyPublisher()
    }
}

func fileURL(file: String) -> URL {
    return Bundle(for: TestModelData.self).url(forResource: file, withExtension: "json")!
}

func decode<T: Decodable>(_ type: T.Type, from url: URL, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T {

    guard let data = try? Data(contentsOf: url) else {
        fatalError("Failed to load \(url.absoluteString) from bundle.")
    }

    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = dateDecodingStrategy
    decoder.keyDecodingStrategy = keyDecodingStrategy

    do {
        return try decoder.decode(T.self, from: data)
    } catch DecodingError.keyNotFound(let key, let context) {
        fatalError("Failed to decode \(url.absoluteString) from bundle due to missing key '\(key.stringValue)' not found – \(context.debugDescription)")
    } catch DecodingError.typeMismatch(_, let context) {
        fatalError("Failed to decode \(url.absoluteString) from bundle due to type mismatch – \(context.debugDescription)")
    } catch DecodingError.valueNotFound(let type, let context) {
        fatalError("Failed to decode \(url.absoluteString) from bundle due to missing \(type) value – \(context.debugDescription)")
    } catch DecodingError.dataCorrupted(_) {
        fatalError("Failed to decode \(url.absoluteString) from bundle because it appears to be invalid JSON")
    } catch {
        fatalError("Failed to decode \(url.absoluteString) from bundle: \(error.localizedDescription)")
    }
}

//
//  CharacterRepo.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import Foundation
import Combine

struct CharacterRepo: CharacterListRepository {
    let name: String
    
    func characterListPublisher() -> AnyPublisher<[Character], HTTPError> {
        return APIClient.shared.getCharacterByName(name: self.name)
    }
}

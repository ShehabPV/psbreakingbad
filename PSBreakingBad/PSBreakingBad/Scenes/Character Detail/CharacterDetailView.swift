//
//  CharacterDetailView.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import SwiftUI
import Kingfisher

struct CharacterDetailView: View {
    @ObservedObject var viewModel: CharactersViewModel
    
    var body: some View {
        Form {
            HStack {
                Spacer()
                KFImage(URL(string: viewModel.imageURLString))
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(16.0)
                    .frame(width: 200, height: 200)
                    .clipped()
                Spacer()
            }
            .background(Color.clear)
            .padding()
            
            Section() {
                DetailRowView(
                    rowDescription: "Actor",
                    rowValueText: viewModel.actorName
                )
                
                DetailRowView(
                    rowDescription: "Character",
                    rowValueText: viewModel.characterName
                )
                
                DetailRowView(
                    rowDescription: "Nickname",
                    rowValueText: viewModel.characterNickname
                )
                
                DetailRowView(
                    rowDescription: "Birthday",
                    rowValueText: viewModel.characterBirthday
                )
                
                DetailRowView(
                    rowDescription: "Status",
                    rowValueText: viewModel.characterStatus
                )
                
                DetailRowView(
                    rowDescription: "Category",
                    rowValueText: viewModel.characterSeries
                )
                
                DetailRowView(
                    rowDescription: "Seasons",
                    rowValueText: viewModel.characterSeasonAppearences
                )
                
                VStack(alignment: .leading) {
                    Text("Occupation")
                        .font(.caption)
                        .foregroundColor(.gray)
                        .padding(.bottom, 2)
                    
                    ForEach(0..<viewModel.characterOccupations.count) {
                        Text(viewModel.characterOccupations[$0])
                            .font(.body)
                    }
                        
                }
            }
            
            Section(header: Text("Quotes")) {
                if viewModel.quotes.isEmpty {
                    Text("No quotes available currently")
                } else {
                    ForEach(viewModel.quotes, id: \.quoteId) { quote in
                        VStack(alignment: .leading, spacing: 8.0) {
                            Text(quote.quote)
                            HStack {
                                Text(quote.series)
                                    .font(.caption)
                                    .foregroundColor(.gray)
                                Spacer()
                            }
                        }
                    }
                }
                
                
            }
            
        }
        .onAppear {
            self.viewModel.fetchQuotesForCharacter()
        }
    }
}

//struct CharacterDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        CharacterDetailView()
//    }
//}

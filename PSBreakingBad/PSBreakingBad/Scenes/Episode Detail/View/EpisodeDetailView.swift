//
//  EpisodeDetailView.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import SwiftUI

struct EpisodeDetailView: View {
    
    var viewModel: EpisodesViewModel
    
    var body: some View {
        Form {
            Section(header: Text("Episode Details")) {
                DetailRowView(
                    rowDescription: "Episode",
                    rowValueText: viewModel.episodeTitle
                )
                
                DetailRowView(
                    rowDescription: "Season",
                    rowValueText: viewModel.episodeSeason
                )
                
                DetailRowView(
                    rowDescription: "Episode#",
                    rowValueText: viewModel.episodeNumber
                )
                
                DetailRowView(
                    rowDescription: "Air Date",
                    rowValueText: viewModel.episodeAirDate
                )
                
                DetailRowView(
                    rowDescription: "Series",
                    rowValueText: viewModel.episodeSeries
                )
            }
            
            Section(header: Text("Characters")) {
                ForEach(0..<viewModel.episodeCharacters.count) { number in
                    Text(viewModel.episodeCharacters[number])
                        .font(.body)
                }
            }
        }
    }
}

struct EpisodeDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let episode = Episodes(episodeId: 1, title: "Hello", season: "1", episode: "1", airDate: "08-09-2021", characters: ["John", "Smith"], series: "2")
        let epViewModel = EpisodesViewModel(episode: episode)
        EpisodeDetailView(viewModel: epViewModel)
    }
}

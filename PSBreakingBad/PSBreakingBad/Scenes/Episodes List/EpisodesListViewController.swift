//
//  ViewController.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import UIKit
import Combine
import SwiftUI

class EpisodesListViewController: UIViewController {

    private let tableView = UITableView(frame: .zero)
    private let refreshControl = UIRefreshControl(frame: .zero)
    private var segmentedControl: UISegmentedControl?
    
    private let segmentedItems = [
        EpisodesSeriesModel.BreakingBad.rawValue,
        EpisodesSeriesModel.BetterCallSaul.rawValue
    ]
    
    private var diffableDatasource: UITableViewDiffableDataSource<Int, EpisodesViewModel>!
    private var cancellables = Set<AnyCancellable>()
    
    private let viewModel = EpisodesListViewModel(series: .BreakingBad)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.configureRefreshControl()
        self.configureDiffableDatasource()
        self.configureSegmentedControl()
        
        self.navigationController?.navigationBar.backgroundColor = .white
        
        viewModel.$showSpinner
            .sink { [unowned self] in
                if $0 {
                    let indicator = UIActivityIndicatorView()
                    navigationItem.rightBarButtonItem  = UIBarButtonItem(customView: indicator)
                    indicator.startAnimating()
                    refreshControl.beginRefreshing()
                } else {
                    navigationItem.rightBarButtonItem = nil
                    refreshControl.endRefreshing()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$httpError
            .sink { [unowned self] error in
                guard let httpError = error else { return }
                self.handle(httpError)
            }
            .store(in: &cancellables)
        
        viewModel.snapshotPublisher
            .sink { [unowned self] snapshot in
                diffableDatasource.apply(snapshot, animatingDifferences: false)
            }
            .store(in: &cancellables)
        
    }

    private func configureTableView() {
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")    }
    
    private func configureRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshData(refreshControl: UIRefreshControl) {
        self.viewModel.updateEpisodes()
    }
    
    private func configureDiffableDatasource() {
        diffableDatasource = UITableViewDiffableDataSource(
            tableView: tableView, cellProvider: {
                (tv, indexPath, viewModel) -> UITableViewCell? in
                
                let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
                cell.textLabel?.text = viewModel.episodeTitle
                cell.detailTextLabel?.text = viewModel.episodeSubtitleText
                return cell
        })
    }
    
    private func configureSegmentedControl() {
        let segControl = UISegmentedControl(items: self.segmentedItems)
        segControl.backgroundColor = UIColor.gray
        segControl.selectedSegmentTintColor = UIColor.white
        segControl.selectedSegmentIndex = 0
        segControl.translatesAutoresizingMaskIntoConstraints = false
        segControl.addTarget(
            self,
            action: #selector(self.segmentValueDidChange(_:)),
            for: .valueChanged
        )
        self.view.addSubview(segControl)
        
        NSLayoutConstraint.activate([
            view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: segControl.topAnchor),
            segControl.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            segControl.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: segControl.bottomAnchor)
        ])
        self.segmentedControl = segControl
    }
    
    @objc func segmentValueDidChange(_ segmentedControl: UISegmentedControl) {
        guard let model = EpisodesSeriesModel(rawValue: segmentedItems[segmentedControl.selectedSegmentIndex]) else { return }
        viewModel.didSelectSeriesModel(model: model)
    }

}

extension EpisodesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = self.diffableDatasource.itemIdentifier(for: indexPath) else {
            return
        }
        let swiftUIView = EpisodeDetailView(viewModel: viewModel)
        let hostingController = UIHostingController(rootView: swiftUIView)
        self.navigationController?.pushViewController(hostingController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//
//  EpisodesViewModel.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import Foundation
import Combine

struct EpisodesViewModel: Equatable, Hashable {
    private let episode: Episodes
    
    init(episode: Episodes) {
        self.episode = episode
    }
    
    static func == (lhs: EpisodesViewModel, rhs: EpisodesViewModel) -> Bool {
        lhs.episode.episodeId == rhs.episode.episodeId
    }
    
    func hash(into hasher: inout Hasher) {
        episode.hash(into: &hasher)
    }
    
    var episodeTitle: String {
        return episode.title
    }
    
    var episodeSeason: String {
        return episode.season
    }
    
    var episodeAirDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
        guard let date = dateFormatter.date(from: episode.airDate) else {
            return episode.airDate
        }
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    var episodeNumber: String {
        return episode.episode
    }
    
    var episodeSeries: String {
        return episode.series
    }
    
    var episodeCharacters: [String] {
        return episode.characters
    }
    
    var episodeSubtitleText: String {
        return "Season \(episodeSeason)" + " - " + "Episode \(episodeNumber)"
    }
}

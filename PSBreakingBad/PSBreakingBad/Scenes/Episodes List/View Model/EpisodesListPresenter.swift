//
//  EpisodesListPresenter.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import Combine
import UIKit

enum EpisodesSeriesModel: String {
    case BreakingBad = "Breaking Bad"
    case BetterCallSaul = "Better Call Saul"
    
    func returnUseCase() -> EpisodesListUseCase {
        return EpisodesListUseCase(repo: EpisodesListRepo(series: self.rawValue))
    }
}

class EpisodesListViewModel {
    
    private var useCase: EpisodesListUseCase
    private var series: EpisodesSeriesModel
    
    private var cancellables: Set<AnyCancellable> = []
    
    @Published
    private var episodes: [Episodes] = []
    
    @Published
    var showSpinner: Bool = false
    
    @Published
    var httpError: HTTPError?
    
    init(series: EpisodesSeriesModel) {
        self.useCase = series.returnUseCase()
        self.series = series
        
        self.updateEpisodes()
    }
    
    func updateEpisodes() {
        self.showSpinner = true
        self.useCase.getEpisodesListPublisher()
            .sink(receiveCompletion: { completion in
                self.showSpinner = false
                switch completion {
                case .failure(let error):
                    self.httpError = error
                default:
                    print("Received completion ")
                }
            }, receiveValue: { allEpisodes in
                self.showSpinner = false
                self.episodes = allEpisodes
            })
            .store(in: &cancellables)
    }
    
    func didSelectSeriesModel(model: EpisodesSeriesModel) {
        self.series = model
        self.useCase = self.series.returnUseCase()
        self.updateEpisodes()
    }
    
    private var episodesViewModel: AnyPublisher<[EpisodesViewModel], Never> {
        self.$episodes
            .map { episodes in
                episodes.map { episode in
                    EpisodesViewModel(episode: episode)
                }
            }
            .eraseToAnyPublisher()
    }
    
    var snapshotPublisher: AnyPublisher<NSDiffableDataSourceSnapshot<Int, EpisodesViewModel>, Never> {
        episodesViewModel
            .map { episodes in
                var snapshot = NSDiffableDataSourceSnapshot<Int, EpisodesViewModel>()
                snapshot.appendSections([1])
                snapshot.appendItems(episodes)
                return snapshot
            }
            .eraseToAnyPublisher()
    } 
}

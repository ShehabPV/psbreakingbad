//
//  EpisodesListUseCase.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import Combine

struct EpisodesListUseCase {
    
    let repo: EpisodesListRepository
    
    func getEpisodesListPublisher() -> AnyPublisher<[Episodes], HTTPError> {
        return repo.episodesListPublisher()
    }
    
}

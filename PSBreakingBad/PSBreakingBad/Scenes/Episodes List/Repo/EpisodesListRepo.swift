//
//  EpisodesListRepo.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import Combine

protocol EpisodesListRepository {
    func episodesListPublisher() -> AnyPublisher<[Episodes], HTTPError>
}

struct EpisodesListRepo: EpisodesListRepository {
    
    let series: String
    
    init(series: String) {
        self.series = series
    }
    
    func episodesListPublisher() -> AnyPublisher<[Episodes], HTTPError> {
        return APIClient.shared.getEpisodeBy(series: self.series)
    }
}

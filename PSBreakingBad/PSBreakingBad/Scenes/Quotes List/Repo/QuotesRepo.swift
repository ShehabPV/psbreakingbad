//
//  QuotesRepo.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import Foundation
import Combine

struct AllQuotesRepo: QuoteListRepository {
    func quoteListPublisher() -> AnyPublisher<[Quote], HTTPError> {
        return APIClient.shared.getAllQuotes()
    }
}

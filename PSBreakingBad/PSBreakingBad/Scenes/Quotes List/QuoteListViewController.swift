//
//  QuoteListViewController.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import UIKit
import Combine

class QuoteListViewController: UIViewController {

    private let tableView = UITableView(frame: .zero)
    private let refreshControl = UIRefreshControl(frame: .zero)

    private var diffableDatasource: UITableViewDiffableDataSource<Int, QuotesViewModel>!
    private var cancellables = Set<AnyCancellable>()
    
    private let viewModel = AllQuotesListViewModel(
        useCase: QuoteListUseCase(
            repo: AllQuotesRepo()
        )
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.configureRefreshControl()
        self.configureDiffableDatasource()
        
        self.navigationController?.navigationBar.backgroundColor = .white
        
        viewModel.$showSpinner
            .sink { [unowned self] in
                if $0 {
                    let indicator = UIActivityIndicatorView()
                    navigationItem.rightBarButtonItem  = UIBarButtonItem(customView: indicator)
                    indicator.startAnimating()
                    refreshControl.beginRefreshing()
                } else {
                    navigationItem.rightBarButtonItem = nil
                    refreshControl.endRefreshing()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$httpError
            .sink { [unowned self] error in
                guard let httpError = error else { return }
                self.handle(httpError)
            }
            .store(in: &cancellables)
        
        viewModel.snapshotPublisher
            .sink { [unowned self] snapshot in
                diffableDatasource.apply(snapshot, animatingDifferences: false)
            }
            .store(in: &cancellables)
        
    }

    private func configureTableView() {
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
    }
    
    private func configureDiffableDatasource() {
        diffableDatasource = UITableViewDiffableDataSource(
            tableView: tableView, cellProvider: {
                (tv, indexPath, viewModel) -> UITableViewCell? in
                
                let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.text = viewModel.quoteText
                cell.detailTextLabel?.text = viewModel.author + " - " + viewModel.series
                return cell
        })
    }
    
    private func configureRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshData(refreshControl: UIRefreshControl) {
        self.viewModel.updateQuotes()
    }
}

extension QuoteListViewController: UITableViewDelegate {}

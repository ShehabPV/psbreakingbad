//
//  AllQuotesListViewModel.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import Foundation
import Combine
import UIKit

class AllQuotesListViewModel {
    
    private var useCase: QuoteListUseCase
    
    private var cancellables: Set<AnyCancellable> = []
    
    @Published
    private var quotes: [Quote] = []
    
    @Published
    var showSpinner: Bool = false
    
    @Published
    var httpError: HTTPError?
    
    init(useCase: QuoteListUseCase) {
        self.useCase = useCase
        self.updateQuotes()
    }
    
    func updateQuotes() {
        self.showSpinner = true
        self.useCase.getQuoteListPublisher()
            .sink(receiveCompletion: { completion in
                self.showSpinner = false
                switch completion {
                case .failure(let error):
                    self.httpError = error
                default:
                    print("Received completion ")
                }
            }, receiveValue: { allQuotes in
                self.showSpinner = false
                self.quotes = allQuotes
            })
            .store(in: &cancellables)
    }
    
    private var quotesViewModel: AnyPublisher<[QuotesViewModel], Never> {
        self.$quotes
            .map { quotes in
                quotes.map { quote in
                    QuotesViewModel(quote: quote)
                }
            }
            .eraseToAnyPublisher()
    }
    
    var snapshotPublisher: AnyPublisher<NSDiffableDataSourceSnapshot<Int, QuotesViewModel>, Never> {
        quotesViewModel
            .map { quote in
                var snapshot = NSDiffableDataSourceSnapshot<Int, QuotesViewModel>()
                snapshot.appendSections([1])
                snapshot.appendItems(quote)
                return snapshot
            }
            .eraseToAnyPublisher()
    }
}

struct QuotesViewModel: Equatable, Hashable {
    private let quote: Quote
    
    init(quote: Quote) {
        self.quote = quote
    }
    
    static func == (lhs: QuotesViewModel, rhs: QuotesViewModel) -> Bool {
        lhs.quote.quoteId == rhs.quote.quoteId
    }
    
    func hash(into hasher: inout Hasher) {
        quote.hash(into: &hasher)
    }
    
    var quoteText: String {
        return quote.quote
    }
    
    var author: String {
        return quote.author
    }
    
    var series: String {
        return quote.series
    }
}

//
//  CharactersCollectionLayoutManager.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import UIKit

struct CharactersCollectionLayoutManager {
    func generateLayout() -> UICollectionViewLayout {
        return UICollectionViewCompositionalLayout(section: self.createCharactersSection())
    }
    
    private func createCharactersSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(0.33),
            heightDimension: .absolute(150)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .absolute(150)
        )
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 3)
        
        let section = NSCollectionLayoutSection(group: group)
        return section
    }
}

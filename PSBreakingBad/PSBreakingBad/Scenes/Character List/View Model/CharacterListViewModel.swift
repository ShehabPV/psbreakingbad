//
//  CharacterListViewModel.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import UIKit
import Combine

class CharactersListViewModel {
    
    private let useCase: CharacterListUseCase
    
    private var cancellables: Set<AnyCancellable> = []
    
    @Published
    private var characters: [Character] = []
    
    @Published
    var showSpinner: Bool = false
    
    @Published
    var httpError: HTTPError?
    
    init(useCase: CharacterListUseCase) {
        self.useCase = useCase
        
        self.updateCharacters()
    }
    
    func updateCharacters() {
        self.showSpinner = true
        self.useCase.getCharacterListPublisher()
            .sink(receiveCompletion: { completion in
                self.showSpinner = false
                switch completion {
                case .failure(let error):
                    self.httpError = error
                default:
                    print("Received completion ")
                }
            }, receiveValue: { allCharacters in
                self.showSpinner = false
                self.characters = allCharacters
            })
            .store(in: &cancellables)
    }
    
    private var charactersViewModel: AnyPublisher<[CharactersViewModel], Never> {
        self.$characters
            .map { characters in
                characters.map { character in
                    CharactersViewModel(character: character)
                }
            }
            .eraseToAnyPublisher()
    }
    
    var snapshotPublisher: AnyPublisher<NSDiffableDataSourceSnapshot<Int, CharactersViewModel>, Never> {
        charactersViewModel
            .map { characters in
                var snapshot = NSDiffableDataSourceSnapshot<Int, CharactersViewModel>()
                snapshot.appendSections([1])
                snapshot.appendItems(characters)
                return snapshot
            }
            .eraseToAnyPublisher()
    }
}

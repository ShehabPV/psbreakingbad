//
//  CharactersViewModel.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import Foundation
import Combine

class CharactersViewModel: Equatable, Hashable, ObservableObject {
    private let character: Character
    var quoteListUseCase: QuoteListUseCase?
    
    private var cancellables: Set<AnyCancellable> = []
    
    @Published
    var quotes: [Quote] = []
    
    init(character: Character) {
        self.character = character
    }
    
    static func == (lhs: CharactersViewModel, rhs: CharactersViewModel) -> Bool {
        lhs.character.charId == rhs.character.charId
    }
    
    func hash(into hasher: inout Hasher) {
        character.hash(into: &hasher)
    }
    
    func fetchQuotesForCharacter() {
        self.quoteListUseCase?.getQuoteListPublisher()
            .replaceError(with: [])
            .sink(receiveValue: { allQuotes in
                self.quotes = allQuotes
            })
            .store(in: &cancellables)
    }
    
    var imageURLString: String {
        return character.img
    }
    
    var actorName: String {
        return character.portrayed
    }
    
    var characterName: String {
        return character.name
    }
    
    var characterBirthday: String {
        return character.birthday
    }
    
    var characterNickname: String {
        return character.nickname
    }
    
    var characterStatus: String {
        return character.status
    }
    
    var characterSeries: String {
        return character.category
    }
    
    var characterSeasonAppearences: String {
        return character.appearance.compactMap{ String($0) }.joined(separator: ", ")
    }
    
    var characterOccupations: [String] {
        return character.occupation
    }
}

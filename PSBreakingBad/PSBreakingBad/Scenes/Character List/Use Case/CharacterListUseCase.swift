//
//  CharacterListUseCase.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import Combine

struct CharacterListUseCase {
    
    let repo: CharacterListRepository
    
    func getCharacterListPublisher() -> AnyPublisher<[Character], HTTPError> {
        return repo.characterListPublisher()
    }
    
}

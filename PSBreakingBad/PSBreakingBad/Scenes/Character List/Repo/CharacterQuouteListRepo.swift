//
//  CharacterQuouteListRepo.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import Foundation
import Combine

protocol QuoteListRepository {
    func quoteListPublisher() -> AnyPublisher<[Quote], HTTPError>
}

struct QuoteListRepo: QuoteListRepository {
    let name: String
    
    func quoteListPublisher() -> AnyPublisher<[Quote], HTTPError> {
        return APIClient.shared.getQuoteByCharacter(name: self.name)
    }
}

struct QuoteListUseCase {
    
    let repo: QuoteListRepository
    
    func getQuoteListPublisher() -> AnyPublisher<[Quote], HTTPError> {
        return repo.quoteListPublisher()
    }
    
}

//
//  CharacterListRepo.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import Combine

protocol CharacterListRepository {
    func characterListPublisher() -> AnyPublisher<[Character], HTTPError>
}

struct CharacterListRepo: CharacterListRepository {
    func characterListPublisher() -> AnyPublisher<[Character], HTTPError> {
        return APIClient.shared.getAllCharacters()
    }
}

//
//  CharactersListViewController.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import UIKit
import Combine
import SwiftUI

class CharactersListViewController: UIViewController {

    private let collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: CharactersCollectionLayoutManager().generateLayout()
    )
    private let refreshControl = UIRefreshControl(frame: .zero)
    
    private var diffableDatasource: UICollectionViewDiffableDataSource<Int, CharactersViewModel>!
    private var cancellables = Set<AnyCancellable>()
    
    private let viewModel = CharactersListViewModel(
        useCase: CharacterListUseCase(repo: CharacterListRepo()
        )
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureCollectionView()
        self.configureRefreshControl()
        self.configureDiffableDatasource()
        
        self.navigationController?.navigationBar.backgroundColor = .white
        
        viewModel.$showSpinner
            .sink { [unowned self] in
                if $0 {
                    let indicator = UIActivityIndicatorView()
                    navigationItem.rightBarButtonItem  = UIBarButtonItem(customView: indicator)
                    indicator.startAnimating()
                    refreshControl.beginRefreshing()
                } else {
                    navigationItem.rightBarButtonItem = nil
                    refreshControl.endRefreshing()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$httpError
            .sink { [unowned self] error in
                guard let httpError = error else { return }
                self.handle(httpError)
            }
            .store(in: &cancellables)
        
        viewModel.snapshotPublisher
            .sink { [unowned self] snapshot in
                diffableDatasource.apply(snapshot, animatingDifferences: false)
            }
            .store(in: &cancellables)
        
    }

    private func configureCollectionView() {
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .systemBackground
        self.view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        collectionView.register(CharacterListCell.self, forCellWithReuseIdentifier: CharacterListCell.reuseID)
    }
    
    private func configureDiffableDatasource() {
        diffableDatasource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { cv, indexPath, viewModel in
                
            let cell = cv.dequeueReusableCell(withReuseIdentifier: CharacterListCell.reuseID, for: indexPath) as! CharacterListCell
            cell.setUp(viewModel: viewModel)
            return cell

        })
    }
    
    private func configureRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc func refreshData(refreshControl: UIRefreshControl) {
        self.viewModel.updateCharacters()
    }
}

extension CharactersListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let viewModel = self.diffableDatasource.itemIdentifier(for: indexPath) else {
            return
        }
        
        let useCase = QuoteListUseCase(repo: QuoteListRepo(name: viewModel.characterName))
        viewModel.quoteListUseCase = useCase
        
        let swiftUIView = CharacterDetailView(viewModel: viewModel)
        let hostingController = UIHostingController(rootView: swiftUIView)
        self.navigationController?.pushViewController(hostingController, animated: true)
    }
}

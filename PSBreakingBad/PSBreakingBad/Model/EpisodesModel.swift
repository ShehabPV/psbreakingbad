//
//  EpisodesModel.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation

struct Episodes: Codable, Hashable, Equatable {
    let episodeId: Int
    let title: String
    let season: String
    let episode: String
    let airDate: String
    let characters: [String]
    let series: String
}

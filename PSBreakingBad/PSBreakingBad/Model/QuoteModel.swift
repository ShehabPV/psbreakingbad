//
//  QuoteModel.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation

struct Quote: Codable, Hashable, Equatable {
    let quoteId: Int
    let quote: String
    let author: String
    let series: String
}

//
//  TabBarController.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = .systemBlue
        viewControllers = [createEpisodesListVC(), createCharacterListVC(), createQuotesListVC()]
    }
    
    func createEpisodesListVC() -> UINavigationController {
        let episodesVC = EpisodesListViewController()
        episodesVC.title = "Episodes"
        episodesVC.tabBarItem = UITabBarItem(
            title: "Episodes",
            image: UIImage(systemName: "video.fill"),
            tag: 0
        )
        
        return UINavigationController(rootViewController: episodesVC)
    }
    
    func createCharacterListVC() -> UINavigationController {
        let episodesVC = CharactersListViewController()
        episodesVC.title = "Characters"
        episodesVC.tabBarItem = UITabBarItem(
            title: "Characters",
            image: UIImage(systemName: "person.fill"),
            tag: 1
        )
        
        return UINavigationController(rootViewController: episodesVC)
    }
    
    func createQuotesListVC() -> UINavigationController {
        let quotesVC = QuoteListViewController()
        quotesVC.title = "Quotes"
        quotesVC.tabBarItem = UITabBarItem(
            title: "Quotes",
            image: UIImage(systemName: "rectangle.3.offgrid.bubble.left.fill"),
            tag: 0
        )
        
        return UINavigationController(rootViewController: quotesVC)
    }
}

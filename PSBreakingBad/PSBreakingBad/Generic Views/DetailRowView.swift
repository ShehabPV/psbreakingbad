//
//  DetailRowView.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import SwiftUI

struct DetailRowView: View {
    let rowDescription: String
    let rowValueText: String
    
    var body: some View {
        HStack {
            Text(rowDescription)
                .font(.caption)
                .foregroundColor(.gray)
            Spacer()
            Text(rowValueText)
                .font(.body)
        }
    }
}

struct DetailRowView_Previews: PreviewProvider {
    static var previews: some View {
        DetailRowView(rowDescription: "Episode", rowValueText: "Pilot")
    }
}

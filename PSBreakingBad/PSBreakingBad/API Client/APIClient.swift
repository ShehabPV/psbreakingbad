//
//  APIClient.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 19/06/2021.
//

import Foundation
import Combine

class APIClient {
    
    static let shared = APIClient()
    
    private let session: URLSession = .shared
    private let baseURL = URL(string: "https://www.breakingbadapi.com/api/")!
        
    func getEpisodeBy(series: String) -> AnyPublisher<[Episodes], HTTPError> {
        let url = baseURL
            .appending(path: "episodes")
            .appending(queryParams: [
                "series": series
            ])
        
        // Using .returnCacheDataElseLoad for cache policy as this data is unlikely
        // to change hence shouldn't need to make a new request everytime.
        let request = URLRequest(
            url: url,
            cachePolicy: .returnCacheDataElseLoad,
            timeoutInterval: 60
        )
        return fetchResource([Episodes].self, with: request)
    }
    
    func getAllCharacters() -> AnyPublisher<[Character], HTTPError> {
        let url = baseURL
            .appending(path: "characters")
            
        let request = URLRequest(
            url: url,
            cachePolicy: .returnCacheDataElseLoad,
            timeoutInterval: 60
        )
        return fetchResource([Character].self, with: request)
    }
    
    //TODO: Use this request to allow a user to go from the episodes detail page
    // to the character detail page when clicking on a character
    func getCharacterByName(name: String) -> AnyPublisher<[Character], HTTPError> {
        let url = baseURL
            .appending(path: "characters")
            .appending(queryParams: [
                "name": name
            ])
            
        let request = URLRequest(
            url: url,
            cachePolicy: .returnCacheDataElseLoad,
            timeoutInterval: 60
        )
        return fetchResource([Character].self, with: request)
    }
    
    func getQuoteByCharacter(name: String) -> AnyPublisher<[Quote], HTTPError> {
        let url = baseURL
            .appending(path: "quote")
            .appending(queryParams: [
                "author": name
            ])
            
        let request = URLRequest(
            url: url,
            cachePolicy: .returnCacheDataElseLoad,
            timeoutInterval: 60
        )
        return fetchResource([Quote].self, with: request)
    }
    
    func getAllQuotes() -> AnyPublisher<[Quote], HTTPError> {
        let url = baseURL
            .appending(path: "quotes")
            
        let request = URLRequest(
            url: url,
            cachePolicy: .returnCacheDataElseLoad,
            timeoutInterval: 60
        )
        return fetchResource([Quote].self, with: request)
    }
    
    private func fetchResource<R: Decodable>(_ type: R.Type, with request: URLRequest) -> AnyPublisher<R, HTTPError> {
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return session.dataTaskPublisher(for: request)
            .assumeHTTP()
            .responseData()
            .decoding(R.self, decoder: decoder)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}

//
//  Ext+UIViewController.swift
//  PSBreakingBad
//
//  Created by Shehab Saqib on 20/06/2021.
//

import UIKit

extension UIViewController {
    func handle(_ error: Error) {
        let alert = UIAlertController(
            title: "An error occured",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
            
        alert.addAction(UIAlertAction(
            title: "Ok",
            style: .default
        ))

        present(alert, animated: true)
    }
}

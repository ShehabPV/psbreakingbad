#  PSBreakingBad 

PSBreakingBad


Decisions 

- Used Combine as my reactive framework of choice 
- Used KingFisher library to allow me load images from urls 
- Used DiffableDatasources for the view controllers on each tab. 
- Episode and Quotes used a basic table view layout with it's respective diffable datasource 
- Characters used a collection view compositional layout to allow me to build a grid layout 
- For the detail pages, I decided to use SwiftUI views that were put into UIHostingControllers to help save time building a UI whilst also showing the skills for it as it'll become more common in modern iOS code. 
- I didn't build a Quotes detail page due to how basic the response is as it could be covered in the quotes screen. What I did instead was on the Character detail page make another request to retrieve quotes for that character. 
- I didn't focus too much on making the UI pretty due to time constraints 
- Added tests to ensure the data is being decoded correctly as the documentation and response are out of sync
- There is a bug in the simulator (At least mine) that the status bar is blacked out but it works fine on a device.

ToDos 


- Better error handling, I show an alert when a HTTPError and have a refresh control so that the user can try to refresh the data. Have better error messages in the alert, which is to format the errors. 
- Try to find a way to make my viewmodels more testable, currently I can test the logic by bringing it into the tests but not the viewmodel it self. 
- I wanted to separate the quotes by series, similar to episodes and try to find a way to use the same view controller. I could inject the view models into the tab bar controller to allow reusability in that instance. 
- Have it so from the episode detail, the user clicks a character we can open the character detail view by making a request on the characters name. 
- Add possible more tests 
- When entering the detail screens, currently I pass the data through due to all the data being returned on the first call. It would be a needless request for a user if there was no caching. In a more real world app where the data on a detail page would usually have more data to be displayed, I would make a request in the .onAppear{} for each screen to show the details data.


I think if I had a day or two more i would be able to implement most of my ToDos if I'm trying to estimate how much work I have. Due to limitations of API there wasn't much that could be done with the data, hence it allowed it to keep the code simple and not have many manipulations.
